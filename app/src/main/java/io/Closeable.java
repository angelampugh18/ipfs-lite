package io;


public interface Closeable {
    boolean isClosed();
}
