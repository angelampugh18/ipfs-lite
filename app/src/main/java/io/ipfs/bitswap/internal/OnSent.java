package io.ipfs.bitswap.internal;

public interface OnSent {
    void invoke();
}
